#!/usr/bin/env bash

set -eu

sed -i 's,/sbin/getty.*$,-/bin/sh,' $TARGET_DIR/etc/inittab
