#!/usr/bin/env bash

set -eu

# <file system> <mount pt> <type> <options> <dump> <pass>
cat >> $TARGET_DIR/etc/fstab <<EOF
none /sys/kernel/debug debugfs default 0 0
/dev/mmcblk0p1 /mnt auto ro 0 0
EOF
