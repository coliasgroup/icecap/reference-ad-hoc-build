#!/usr/bin/env bash

set -eu

# <file system> <mount pt> <type> <options> <dump> <pass>
cat >> $TARGET_DIR/etc/fstab <<EOF
none /sys/kernel/debug debugfs default 0 0
share /mnt 9p trans=virtio,version=9p2000.L,ro 0 0
EOF
